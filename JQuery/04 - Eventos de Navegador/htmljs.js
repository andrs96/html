$(function(){
/*
	$("img").error(function(){						// img de error
		$("img").attr("src","img/erroimg.jpg"); 	// attr é Atributo, caso o codigo nao procure a imagem real ele estará substituindo por uma de error
	});
*/
/*
	$("img").error(function(){						// img de error
		var imagem = $(this).attr("src");
		alert("Imagem Indisponível! " + imagem);	// Mostra o caminho concatenando a variavel
		$("img").attr("src","img/erroimg.jpg");
	});
*/
/*
	$(window).resize(function(){
		$("img").width($(window).width()).height($(window).height());	// Ele pega totalmente a tela **Não aconselho**
	});
*/
/*
	$(window).scroll(function(){					// Ao rolar o scroll a imagem desaparece
		$("img").fadeOut("1000");
	});
*/
/*
	$("body").css({height:"5000px"});
	$(window).scroll(function(){
		var topo = $(window).scrollTop();			// Criando uma variavel topo e colocando o valor da janela no alerta para mostrar o valor
		alert(topo);
	});
*/
	$("body").css({height:"5000px"});				// Aumentando o body
	$(window).scroll(function(){
		var topo = $(window).scrollTop();			// Criando variavel com valor do topo
			if(topo > 600){							// Se a rolagem passar de 600 a imagem some, se não retorna
				$("img").fadeOut("1000");
			}
			else{
				$("img").fadeIn("1000");
			}
	});
});