<html>
<head>
<meta charset="utf-8">
    <title>
    <?php bloginfo('name'); ?>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="wp-content/themes/uploads/2016/07/favicon.icon_.png" type="image/x-icon"/>	<!-- Icone -->
    <link href="<?php bloginfo('stylesheet_url');?>" rel="stylesheet">
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="wp-content/themes/criar-site/js/bootstrap.js"></script>								<!-- Para o JS Funcionar, basta colocar a url após o wp-content -->
	<?php wp_enqueue_script("jquery"); ?>
    <?php wp_head(); ?>
</head>

<body>
<div class="container" id="logo">
    <a href="http://127.0.0.1/UNIFESP/">
        <!-- <img src="url-da-imagem.png"/> -->
    </a>
</div>


<menu id="menuSup">
    <div class="container">
		<?php wp_nav_menu(array('sort_column' => 'menu_order', 'container_class' => 'menu')); ?>
    </div>
</menu>

<div class="container" style="background-color: #444242;">
    <menu id="menuRes">
        <a type="button" data-toggle="collapse" data-target="#menuResponsivo">
            <!-- <img src="url-da-imagem.png" width="92" height="40" /> -->
        </a>
        <div id="menuResponsivo" class="collapse">
            <?php wp_nav_menu(array('sort_column' => 'menu_order', 'container_class' => 'menu')); ?>
        </div>
    </menu>
</div>

<p>Head</p>