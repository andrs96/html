﻿<?php 

function wpbootstrap_scripts_with_jquery()
{
	// Register the script like this for a theme:
	wp_register_script( 'custom-script', get_template_directory_uri() . '/bootstrap/js/bootstrap.js', array( 'jquery' ) );
	// For either a plugin or a theme, you can then enqueue the script:
	wp_enqueue_script( 'custom-script' );
}
add_action( 'wp_enqueue_scripts', 'wpbootstrap_scripts_with_jquery' );

if ( function_exists('register_sidebar') )
	register_sidebar(array(
		'name' => 'Campo de busca',
		'id' => 'search',
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '<h2 style="display:none">',
		'after_title' => '</h2>',
	));	

if ( function_exists('register_sidebar') )
	register_sidebar(array(
		'name' => 'Nuvem de Tags',
		'id' => 'cloud-tags',
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '<h2 style="display:none">',
		'after_title' => '</h2>',
	));	

// Add Shortcode
function timeline_express() {
}
add_shortcode( 'timeline', 'timeline_express' );



// post Menu
add_theme_support('menus');
		 
add_theme_support( 'post-thumbnails' );

add_filter('show_admin_bar', '__return_false');	
?>