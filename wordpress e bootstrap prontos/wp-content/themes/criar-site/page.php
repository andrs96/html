<?php get_header(); ?>

<div class="tituloPag">
  <div class="container">
    <h1>
      <?php the_title(); ?>
    </h1>
    <?php include 'btnVoltar.php' ?>
  </div>
</div>
<div class="container">
  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
  <?php the_content(); ?>
  <?php endwhile; else: ?>
  <p>
    <?php _e('Sorry, this page does not exist.'); ?>
  </p>
  <?php endif; ?>
</div>
<?php include ('footer.php'); ?>
<?php get_footer(); ?>
