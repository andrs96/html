<?php

/*
Single Post Template: [Nome que identifica o template no painel de posts]
Description: Descrição para ajudar na escolha do template
*/

// Chamo a variavel global que me trás as informações do post acessado
global $post;

// Aqui eu pego o array com os dados da categoria do post
$categoria_arr = get_the_category($post->ID);

// Aqui eu pego o slug da primeira categoria do post
$categoria = $categoria_arr[0]->slug;

// inicio o swit que vai verificar a categoria e incluir o arquivo com leyout correspondente
	switch ($categoria) {


	case 'contato':
	include_once TEMPLATEPATH . '/single-contato.php';
    break;
	
	case 'trabalhos':
	include_once TEMPLATEPATH . '/single-trabalhos.php';
    break;

	case 'programa':
	include_once TEMPLATEPATH . '/single-programa.php';
    break;

	case 'congresso':
	include_once TEMPLATEPATH . '/single-programa.php';
    break;

	case 'convidados':
	include_once TEMPLATEPATH . '/single-programa.php';
    break;
	
	case 'expositores':
	include_once TEMPLATEPATH . '/single-programa.php';
    break;
	
	case 'inscricao':
	include_once TEMPLATEPATH . '/single-programa.php';
    break;

    // Se naõ for nenhuma das categorias com layout pré definido chamo o layout padrão

    default:

	include_once TEMPLATEPATH . '/single-full.php';

}


?>
